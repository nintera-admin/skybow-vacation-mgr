// test
function go() {
    getDays().then(function(data){
        console.log(data);
        $('#days').val(data);
    });
    
}

function getDays() {
    var dateStart = $("#date1").val();
    var dateEnd = $("#date2").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var workingdays = $("#workdays").val();
    var firstHalf = $("#firsthalf").prop("checked");
    var lastHalf = $("#lasthalf").prop("checked");

    return VacationManager.computeDays(dateStart, dateEnd, country, state, workingdays, firstHalf, lastHalf);
}

function initForm() {
    $(".datepicker").datepicker({
        dateFormat: "dd.mm.yy"
    });

    $("#btnCalc").hide();

    $("#lang").on("change", function () {
        initForm();
    });

    // fill options
    var lang = $("#lang").val();
    var weekdaynames = lang == "de" ? VacationManager.WEEKDAYNAMES_DE : VacationManager.WEEKDAYNAMES_EN;
    var countries = VacationManager.COUNTRIES;
    var states = VacationManager.STATES;

    $("#state").empty();

    $("#workdays").empty();
    for (var x = 0; x < weekdaynames.length; x++) {
        var opt = new Option(weekdaynames[x], weekdaynames[x]);
        if (x < 5) {
            opt.selected = true;
        }
        $("#workdays").append(opt);
    }

    $("#country").empty();
    $("#country").append(new Option("", ""));
    var start = lang == "de" ? 0 : 3;
    var end = lang == "de" ? 2 : 5;
    for (var x = start; x < end + 1; x++) {
        $("#country").append(new Option(countries[x], countries[x]));
    }
    $("#country").on("change", function () {
        var idx = $(this).prop("selectedIndex");
        var start = 0;
        var end = 15;
        if (idx == 1 || idx == 4) {
            // Germany, nothing to do
        }
        if (idx == 2 || idx == 5) {
            start = 16;
            end = 24;
        }
        if (idx == 3 || idx == 6) {
            start = 25;
            end = states.length;
        }
        $("#state").empty();
        $("#state").append(new Option("", ""));
        for (var x = start; x < end + 1; x++) {
            $("#state").append(new Option(states[x], states[x]));
        }
    })

    $("#state").on("change", function () {
        checkForm();
    });


    // default values
    $("#date1").val(moment(new Date()).format("DD.MM.YYYY")).on("change", function () {
        checkForm();
    });
    $("#date2").val(moment(new Date()).add(7, "days").format("DD.MM.YYYY")).on("change", function () {
        checkForm();
    });
}

function checkForm() {
    var dateStart = $("#date1").val();
    var dateEnd = $("#date2").val();
    var country = $("#country").val();
    var state = $("#state").val();

    if (dateStart != "" && dateEnd != "" && country != "" && state != "") {
        $("#btnCalc").show();
    } else {
        $("#btnCalc").hide();
    }
}

$(document).ready(function () {
    initForm();
});