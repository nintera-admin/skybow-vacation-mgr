var url = "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js";
var fileName = "momentjs";
var moduleCache = window.module;
var defineCache = window.define;
window.module = undefined;
window.define = undefined;
window.SP.SOD.registerSod(fileName, url);
window.LoadSodByKey(fileName, null, true);
window.module = moduleCache;
window.define = defineCache;

var VacationManager = {

  // globals
  HOLIDAYS_ENDPOINT: "https://date.nager.at/api/v3/PublicHolidays",
  WEEKDAYNAMES_EN: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
  WEEKDAYNAMES_DE: ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
  COUNTRIES: ["Deutschland", "Österreich", "Schweiz", "Germany", "Austria", "Switzerland"],
  COUNTRIES_SHORT: ["DE", "AT", "CH", "DE", "AT", "CH"],
  STATES: ["Baden-Württemberg", "Bayern", "Berlin", "Brandenburg", "Bremen", "Hamburg", "Hessen", "Mecklenburg-Vorpommern", "Niedersachsen", "Nordrhein-Westfalen", "Rheinland-Pfalz", "Saarland", "Sachsen", "Sachsen-Anhalt", "Schleswig-Holstein", "Thüringen",
    "Burgenland", "Kärnten", "Niederösterreich", "Oberösterreich", "Salzburg", "Steiermark", "Tirol", "Vorarlberg", "Wien",
    "Aargau", "Appenzell Ausserrhoden", "Appenzell Innerrhoden", "Basel-Landschaft", "Basel-Stadt", "Bern", "Fribourg", "Genève", "Glarus", "Graubünden", "Jura", "Luzern", "Neuchâtel", "Nidwalden", "Obwalden", "Schaffhausen", "Schwyz", "Solothurn", "St. Gallen", "Thurgau", "Ticino", "Uri", "Valais", "Vaud", "Zug", "Zürich"
  ],
  STATES_SHORT: ["BW", "BY", "BE", "BB", "HB", "HH", "HE", "MV", "NI", "NW", "RP", "SL", "SN", "ST", "SW", "TH",
    "Bgld.", "Ktn.", "NÖ", "OÖ", "Sbg.", "Stmk.", "T", "Vbg.", "W",
    "AG", "AR", "AI", "BL", "BS", "BE", "FR", "GE", "GL", "GR", "JU", "LU", "NE", "NW", "OW", "SG", "SH", "SZ", "SO", "TG", "TI", "UR", "VD", "VS", "ZG", "ZH"
  ],

  // main function called from form
  computeDays: function (dateStart, dateEnd, country, state, workingdays, firstHalf, lastHalf, callback) {
    country = VacationManager.COUNTRIES_SHORT[VacationManager.COUNTRIES.indexOf(country)];
    state = VacationManager.STATES_SHORT[VacationManager.STATES.indexOf(state)];
    // call the REST service
    var year = new Date().getFullYear();
    return new Promise(function(resolve, reject){
      $.ajax({
        async: true,
        method: 'GET',
        dataType: 'json',
        url: VacationManager.HOLIDAYS_ENDPOINT + "/" + year + "/" + country,
        success: function(resp){
          resolve(VacationManager.getDays(workingdays, resp, dateStart, dateEnd, firstHalf, lastHalf, country, state));
        },
        error: function(error){
          reject(JSON.stringify(error));
        }
      })
    });
  },

  // check if a holiday is within date range
  isHoliday: function (dateToCheck, holidays, country, state) {
    var dat = moment(dateToCheck).format("YYYY-MM-DD");
    // console.log("CHECK: " + dat);
    for (var x = 0; x < holidays.length; x++) {
      var holiday = holidays[x];
      if (holiday.hasOwnProperty("date")) {
        //console.log(dateToCheck.toString(), holiday.date);
        if (dat == holiday.date) {
          //console.log("checking for state")
          // holiday, check if in state
          if (holiday.counties != null) {
            var counties = holiday.counties;
            for (var y = 0; y < counties.length; y++) {
              var county = counties[y];
              if (county == country + "-" + state) {
                return true;
              }
            }
          } else {
            return true;
          }
        }
      }
    }
    return false;
  },

  isWorkday: function (dDate1, workingdays) {
    return (VacationManager.inArray(VacationManager.WEEKDAYNAMES_EN[dDate1.getDay() - 1], workingdays) || VacationManager.inArray(VacationManager.WEEKDAYNAMES_DE[dDate1.getDay() - 1], workingdays));
  },

  // calc days based on workdays and holidays
  getDays: function (workingdays, holidays, dateStart, dateEnd, firstHalf, lastHalf, country, state) {
    var d1 = Date.parse(moment(dateStart, "DD.MM.YYYY"));
    var d2 = Date.parse(moment(dateEnd, "DD.MM.YYYY"));
    var dDate1 = new Date(d1);
    var dDate2 = new Date(d2);
    dDate1.setHours(0, 0, 0, 1);
    // Start just after midnight
    dDate2.setHours(23, 59, 59, 999);
    // End just before midnight
    var diff = dDate2 - dDate1;
    // Milliseconds between datetime objects    
    var days = Math.ceil(diff / (86400 * 1000));

    // Remove days not a working day
    var countNoWorkingDays = 0;
    for (var index = 0; index < days; index++) {
      if (VacationManager.isHoliday(dDate1, holidays, country, state) || !VacationManager.isWorkday(dDate1, workingdays)) {
        countNoWorkingDays++;
      }
      dDate1.setDate(dDate1.getDate() + 1); // increment by one day
    }
    days = countNoWorkingDays > 0 ? days - countNoWorkingDays : days;
    // Remove half days
    days = lastHalf ? days - 0.5 : days;
    days = firstHalf ? days - 0.5 : days;

    return days;
  },

  inArray: function (target, array) {
    /* Caching array.length doesn't increase the performance of the for loop on V8 (and probably on most of other major engines) */
    for (var i = 0; i < array.length; i++) {
      if (array[i] === target) {
        return true;
      }
    }
    return false;
  }
};

if(![[DateStart]]||![[DateEnd]]||[[LookupToVacationAccounts.Country.Value]]==""||[[LookupToVacationAccounts.State.Value]]==""){
  return 0;
} else {
  var dateStart = [[DateStart]];
  var dateEnd = [[DateEnd]];
  var country = [[LookupToVacationAccounts.Country.Value]];
  var state = [[LookupToVacationAccounts.State.Value]];
  var workingdays = [[LookupToVacationAccounts.WorkingDays]];
  var firstHalf = [[FirstDayHalf]];
  var lastHalf = [[LastDayHalf]];
  
  return VacationManager.computeDays(dateStart, dateEnd, country, state, workingdays, firstHalf, lastHalf);
}